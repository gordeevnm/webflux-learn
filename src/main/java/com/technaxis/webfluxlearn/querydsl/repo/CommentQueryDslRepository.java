package com.technaxis.webfluxlearn.querydsl.repo;

import com.querydsl.jpa.impl.JPAQuery;
import com.technaxis.webfluxlearn.jpa.model.Comment;
import com.technaxis.webfluxlearn.jpa.model.QComment;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
@Repository
@RequiredArgsConstructor
public class CommentQueryDslRepository {
    private final EntityManager entityManager;

    public List<Comment> findAllByTextLike(String text, Pageable pageable) {
        final var query = new JPAQuery<Comment>(entityManager)
                .from(QComment.comment)
                .where(QComment.comment.text.like(text))
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset());
        return query.fetch();
    }
}
