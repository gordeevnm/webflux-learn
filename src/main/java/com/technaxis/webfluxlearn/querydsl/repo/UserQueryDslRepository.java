package com.technaxis.webfluxlearn.querydsl.repo;

import com.querydsl.jpa.impl.JPAQuery;
import com.technaxis.webfluxlearn.jpa.model.QUser;
import com.technaxis.webfluxlearn.jpa.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
@Repository
@RequiredArgsConstructor
public class UserQueryDslRepository {
    private final EntityManager entityManager;

    public User findById(long id) {
        final var query = new JPAQuery<User>(entityManager)
                .from(QUser.user)
                .where(QUser.user.id.eq(id))
                .limit(1);
        return query.fetchFirst();
    }
}
