package com.technaxis.webfluxlearn.jpa.repo;

import com.technaxis.webfluxlearn.jpa.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface PostJpaRepository extends JpaRepository<Post, Long> {
}
