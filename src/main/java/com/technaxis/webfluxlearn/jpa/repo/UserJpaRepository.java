package com.technaxis.webfluxlearn.jpa.repo;

import com.technaxis.webfluxlearn.jpa.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface UserJpaRepository extends JpaRepository<User, Long> {
    Page<User> findAllByNameLike(String query, Pageable page);
}
