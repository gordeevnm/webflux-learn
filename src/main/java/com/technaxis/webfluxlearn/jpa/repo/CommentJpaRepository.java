package com.technaxis.webfluxlearn.jpa.repo;

import com.technaxis.webfluxlearn.jpa.model.Comment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface CommentJpaRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByTextLike(String text, Pageable pageable);
}
