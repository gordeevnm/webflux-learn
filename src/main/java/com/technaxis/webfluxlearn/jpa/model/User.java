package com.technaxis.webfluxlearn.jpa.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
@Entity(name = "app_user")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_generator")
    @SequenceGenerator(name = "user_id_generator", sequenceName = "user_id_seq", allocationSize = 1)
    private Long id;
    private String name;
    private Timestamp birth;
    private String about;
}
