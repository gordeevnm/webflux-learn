package com.technaxis.webfluxlearn.jpa.model;

import lombok.*;

import javax.persistence.*;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
@Entity(name = "post")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_id_generator")
    @SequenceGenerator(name = "post_id_generator", sequenceName = "post_id_seq", allocationSize = 1)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;
    private String title;
    private String text;
}
