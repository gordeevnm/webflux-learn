package com.technaxis.webfluxlearn.mongo;

import com.technaxis.webfluxlearn.mongo.model.Comment;
import com.technaxis.webfluxlearn.mongo.model.Post;
import com.technaxis.webfluxlearn.mongo.model.User;
import com.technaxis.webfluxlearn.mongo.repo.CommentMongoRepository;
import com.technaxis.webfluxlearn.mongo.repo.PostMongoRepository;
import com.technaxis.webfluxlearn.mongo.repo.UserMongoRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 22.02.2020
 */
@Service
@RequiredArgsConstructor
public class MongoTestDataService {
    private final CommentMongoRepository commentRepository;
    private final PostMongoRepository postRepository;
    private final UserMongoRepository userRepository;

    @SneakyThrows
    public void generateAll() {
        final ExecutorService executor = Executors.newFixedThreadPool(32);
        for (int i = 0; i < 200; i++) {
            executor.submit(() -> {
                List<User> users = generateUsers(500);
                userRepository.saveAll(users);
                List<Post> posts = generatePosts(500, users);
                postRepository.saveAll(posts);
                List<Comment> comments = generateComments(500, users, posts);
                commentRepository.saveAll(comments);
            });
        }
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
    }

    private final AtomicLong userIdSeq = new AtomicLong(1);
    private final AtomicLong postIdSeq = new AtomicLong(1);
    private final AtomicLong commentIdSeq = new AtomicLong(1);

    public List<Post> generatePosts(int count, List<User> authors) {
        List<Post> posts = new ArrayList<>();
        for (int j = 0; j < count; j++) {
            posts.add(Post.builder()
                    .id(postIdSeq.getAndIncrement())
                    .author(authors.get(random.nextInt(authors.size())))
                    .title(rndString(10, 40))
                    .text(rndString(10, 1000))
                    .build());
        }
        return posts;
    }

    public List<Comment> generateComments(int count, List<User> authors, List<Post> posts) {
        List<Comment> comments = new ArrayList<>();
        for (int j = 0; j < count; j++) {
            comments.add(Comment.builder()
                    .id(commentIdSeq.getAndIncrement())
                    .author(authors.get(random.nextInt(authors.size())))
                    .post(posts.get(random.nextInt(posts.size())))
                    .text(rndString(10, 200))
                    .build());
        }
        return comments;
    }

    public List<User> generateUsers(int count) {
        List<User> users = new ArrayList<>();
        for (int j = 0; j < count; j++) {
            users.add(User.builder()
                    .id(userIdSeq.getAndIncrement())
                    .name(rndString(10, 100))
                    .about(rndString(10, 1000))
                    .birth(new Timestamp(rndLong(Integer.MAX_VALUE)))
                    .build());
        }
        return users;
    }

    private static final Random random = new Random();
    private static final String chars = "qwetuiopasdfghjklzxcvbnm 1234567890 -    `\"`_+     ";

    private String rndString(int minLength, int maxLength) {
        int l = random.nextInt(maxLength - minLength) + minLength;
        StringBuilder sb = new StringBuilder(l);
        for (int i = 0; i < l; i++) {
            sb.append(chars.charAt(Math.abs(random.nextInt()) % chars.length()));
        }
        return sb.toString();
    }

    private long rndLong(int mod) {
        return random.nextLong() % mod;
    }
}
