package com.technaxis.webfluxlearn.mongo.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
@Document(collection = "comments")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Comment {
    @Id
    @Indexed
    private Long id;
    @Indexed
    private User author;
    @Indexed
    private Post post;
    @Indexed
    private String text;
}
