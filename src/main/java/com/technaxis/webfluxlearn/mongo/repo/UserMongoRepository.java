package com.technaxis.webfluxlearn.mongo.repo;

import com.technaxis.webfluxlearn.mongo.model.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface UserMongoRepository extends ReactiveMongoRepository<User, Long> {
}
