package com.technaxis.webfluxlearn.mongo.repo;

import com.technaxis.webfluxlearn.mongo.model.Comment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface CommentMongoRepository extends ReactiveMongoRepository<Comment, Long> {
    @Query("{}")
    Flux<Comment> findAll(Pageable pageable);
}
