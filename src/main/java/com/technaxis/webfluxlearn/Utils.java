package com.technaxis.webfluxlearn;

import java.util.Random;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 24.02.2020
 */
public class Utils {
    public static final Random random = new Random();

    public static int getRandomInt(int min, int max) {
        return Math.abs(random.nextInt()) % (max - min) + min;
    }

    private static final String CHARS = "qwetuiopasfghjklzxcbnm";

    public static char getRandomChar() {
        return CHARS.charAt(getRandomInt(0, CHARS.length()));
    }
}
