package com.technaxis.webfluxlearn;

import io.r2dbc.pool.ConnectionPool;
import io.r2dbc.pool.ConnectionPoolConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 02.03.2020
 */
@Configuration
@EnableR2dbcRepositories
public class R2dbcConfig extends AbstractR2dbcConfiguration {
    @Value("${r2dbc.host}")
    private String host;
    @Value("${r2dbc.port}")
    private int port;
    @Value("${r2dbc.database}")
    private String database;
    @Value("${r2dbc.username}")
    private String username;
    @Value("${r2dbc.password}")
    private String password;
    @Value("${r2dbc.init-size}")
    private int poolInitSize;
    @Value("${r2dbc.max-size}")
    private int poolMaxSize;

    @Override
    @Bean
    public ConnectionFactory connectionFactory() {
        return new ConnectionPool(ConnectionPoolConfiguration
                .builder(new PostgresqlConnectionFactory(
                        PostgresqlConnectionConfiguration.builder()
                                .host(host)
                                .username(username)
                                .password(password)
                                .port(port)
                                .database(database)
                                .build()))
                .initialSize(poolInitSize)
                .maxSize(poolMaxSize)
                .build());
    }
}
