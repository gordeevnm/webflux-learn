package com.technaxis.webfluxlearn.hell;

import com.technaxis.webfluxlearn.jdbc.model.Comment;
import com.technaxis.webfluxlearn.jdbc.model.User;
import com.technaxis.webfluxlearn.rest.service.ThirdPartyRestService;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 09.03.2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Accessors(chain = true)
public class Hell {
    private User userJdbc;
    private List<Comment> commentsJdbc;
    private com.technaxis.webfluxlearn.jpa.model.User userJpa;
    private List<com.technaxis.webfluxlearn.jpa.model.Comment> commentsJpa;
    private com.technaxis.webfluxlearn.mongo.model.User userMongo;
    private List<com.technaxis.webfluxlearn.mongo.model.Comment> commentsMongo;
    private List<ThirdPartyRestService.City> cities;
}
