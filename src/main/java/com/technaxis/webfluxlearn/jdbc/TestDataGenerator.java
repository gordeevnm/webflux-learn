package com.technaxis.webfluxlearn.jdbc;

import com.technaxis.webfluxlearn.jdbc.model.Comment;
import com.technaxis.webfluxlearn.jdbc.model.Post;
import com.technaxis.webfluxlearn.jdbc.model.User;
import com.technaxis.webfluxlearn.jdbc.repo.CommentJdbcRepository;
import com.technaxis.webfluxlearn.jdbc.repo.PostJdbcRepository;
import com.technaxis.webfluxlearn.jdbc.repo.UserJdbcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 22.02.2020
 */
@Service
@RequiredArgsConstructor
public class TestDataGenerator {
    private final CommentJdbcRepository commentJdbcRepository;
    private final PostJdbcRepository postJdbcRepository;
    private final UserJdbcRepository userJdbcRepository;
    private final Random random = new Random();

    public void generateUsers() {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 100; i++) {
            executor.submit(() -> {
                List<User> users = new ArrayList<>();
                for (int j = 0; j < 10000; j++) {
                    users.add(User.builder()
                            .name(rndString(10, 100))
                            .about(rndString(10, 1000))
                            .birth(new Timestamp(rndLong(Integer.MAX_VALUE)))
                            .build());
                }
                userJdbcRepository.saveAll(users);
            });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void generatePosts(long[] authorId) {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        for (int i = 0; i < 100; i++) {
            executor.submit(() -> {
                List<Post> posts = new ArrayList<>();
                for (int j = 0; j < 10000; j++) {
                    posts.add(Post.builder()
                            .authorId(authorId[Math.abs(random.nextInt()) % authorId.length])
                            .title(rndString(10, 40))
                            .text(rndString(10, 1000))
                            .build());
                }
                postJdbcRepository.saveAll(posts);
            });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void generateComments(long[] authorId, long[] postId) {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        for (int i = 0; i < 200; i++) {
            executor.submit(() -> {
                List<Comment> comments = new ArrayList<>();
                for (int j = 0; j < 50000; j++) {
                    comments.add(Comment.builder()
                            .authorId(authorId[Math.abs(random.nextInt()) % authorId.length])
                            .postId(postId[Math.abs(random.nextInt()) % postId.length])
                            .text(rndString(10, 200))
                            .build());
                }
                commentJdbcRepository.saveAll(comments);
            });
        }
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static final String chars = "qwetuiopasdfghjklzxcvbnm 1234567890 -    `\"`_+     ";

    private String rndString(int minLength, int maxLength) {
        int l = random.nextInt(maxLength - minLength) + minLength;
        StringBuilder sb = new StringBuilder(l);
        for (int i = 0; i < l; i++) {
            sb.append(chars.charAt(Math.abs(random.nextInt()) % chars.length()));
        }
        return sb.toString();
    }

    private long rndLong(int mod) {
        return random.nextLong() % mod;
    }
}
