package com.technaxis.webfluxlearn.jdbc.repo;

import com.technaxis.webfluxlearn.jdbc.model.Comment;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface CommentJdbcRepository extends ReactiveCrudRepository<Comment, Long> {
    @Query("select * " +
            "from comment " +
            "where text like :text " +
            "order by id asc " +
            "limit :limit " +
            "offset :offset")
    Flux<Comment> findAllByTextLike(@Param("text") String text,
                                    @Param("limit") int limit,
                                    @Param("offset") int offset);
}
