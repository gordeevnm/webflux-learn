package com.technaxis.webfluxlearn.jdbc.repo;

import com.technaxis.webfluxlearn.jdbc.model.Post;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 18.02.2020
 */
public interface PostJdbcRepository extends ReactiveCrudRepository<Post, Long> {
    @Query("select id from post")
    long[] getAllIds();
}
