package com.technaxis.webfluxlearn;

import com.technaxis.webfluxlearn.hell.Hell;
import com.technaxis.webfluxlearn.jdbc.repo.CommentJdbcRepository;
import com.technaxis.webfluxlearn.jdbc.repo.UserJdbcRepository;
import com.technaxis.webfluxlearn.jpa.model.Comment;
import com.technaxis.webfluxlearn.jpa.model.User;
import com.technaxis.webfluxlearn.jpa.repo.CommentJpaRepository;
import com.technaxis.webfluxlearn.jpa.repo.UserJpaRepository;
import com.technaxis.webfluxlearn.mongo.repo.CommentMongoRepository;
import com.technaxis.webfluxlearn.mongo.repo.UserMongoRepository;
import com.technaxis.webfluxlearn.querydsl.repo.CommentQueryDslRepository;
import com.technaxis.webfluxlearn.querydsl.repo.UserQueryDslRepository;
import com.technaxis.webfluxlearn.rest.service.ThirdPartyRestService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 24.02.2020
 */
@RestController
@RequiredArgsConstructor
public class TestController {
    private final UserQueryDslRepository userQueryDslRepository;
    private final CommentQueryDslRepository commentQueryDslRepository;

    private final UserMongoRepository userMongoRepository;
    private final CommentMongoRepository commentMongoRepository;

    private final UserJpaRepository userJpaRepository;
    private final CommentJpaRepository commentJpaRepository;

    private final UserJdbcRepository userJdbcRepository;
    private final CommentJdbcRepository commentJdbcRepository;

    private final ThirdPartyRestService restService;

    private final Scheduler ioScheduler = Schedulers.newBoundedElastic(
            200,
            1000000,
            "io-scheduler"
    );

    /*
     * **********
     * JPA
     * **********
     * */

    @GetMapping("/jpa/users")
    public Mono<User> findRandomUserJPA() {
        return Mono.from(
                Mono.fromCallable(() -> (long) Utils.getRandomInt(1, 1000000))
                        .map(userJpaRepository::findById)
                        .flatMap(r -> r.map(Mono::just).orElseGet(Mono::empty)))
                .repeatWhenEmpty(Flux::repeat)
                .single()
                .subscribeOn(ioScheduler)
                .take(Duration.ofSeconds(3));
    }

    @GetMapping("/jpa/comments")
    public Flux<Comment> findRandomCommentsJPA() {
        return Flux.fromStream(
                () -> commentJpaRepository.findAllByTextLike(
                        "%" + Utils.getRandomChar() + "%",
                        PageRequest.of(
                                0,
                                Utils.getRandomInt(1, 20)
                        )
                ).stream()
        )
                .subscribeOn(ioScheduler)
                .take(Duration.ofSeconds(3));
    }

    /*
     * **********
     * QueryDsl
     * **********
     * */

    @GetMapping("/querydsl/users")
    public Mono<User> findRandomUserQDSL() {
        return Mono.from(
                Mono.fromCallable(() -> (long) Utils.getRandomInt(1, 1000000))
                        .flatMap(id -> Mono.justOrEmpty(userQueryDslRepository.findById(id))))
                .repeatWhenEmpty(Flux::repeat)
                .single()
                .subscribeOn(ioScheduler);
    }

    @GetMapping("/querydsl/comments")
    public Flux<Comment> findRandomCommentsQDSL() {
        return Flux.fromStream(
                () -> commentQueryDslRepository.findAllByTextLike(
                        "%" + Utils.getRandomChar() + "%",
                        PageRequest.of(
                                0,
                                Utils.getRandomInt(1, 20)
                        )
                ).stream()
        )
                .subscribeOn(ioScheduler)
                .take(Duration.ofSeconds(3));
    }

    /*
     * **********
     * JDBC
     * **********
     * */

    @GetMapping("/jdbc/users")
    public Mono<com.technaxis.webfluxlearn.jdbc.model.User> findRandomUserJDBC() {
        return Mono.from(
                Mono.fromCallable(() -> (long) Utils.getRandomInt(1, 1000000))
                        .flatMap(userJdbcRepository::findById))
                .repeatWhenEmpty(Flux::repeat)
                .single()
                .take(Duration.ofSeconds(3));
    }

    @GetMapping("/jdbc/comments")
    public Flux<com.technaxis.webfluxlearn.jdbc.model.Comment> findRandomCommentsJDBC() {
        return commentJdbcRepository.findAllByTextLike(
                "%" + Utils.getRandomChar() + "%",
                Utils.getRandomInt(1, 1000),
                Utils.getRandomInt(0, 100)
        ).take(Duration.ofSeconds(3));
    }

    /*
     * **********
     * Mongo
     * **********
     * */

    @GetMapping("/mongo/users")
    public Mono<com.technaxis.webfluxlearn.mongo.model.User> findRandomUserMongo() {
        return Mono.from(
                Mono.fromCallable(() -> (long) Utils.getRandomInt(1, 1000000))
                        .flatMap(userMongoRepository::findById))
                .repeatWhenEmpty(Flux::repeat)
                .single()
                .take(Duration.ofSeconds(3));
    }

    @GetMapping("/mongo/comments")
    public Flux<com.technaxis.webfluxlearn.mongo.model.Comment> findRandomCommentsMongo() {
        return commentMongoRepository.findAll(
                PageRequest.of(
                        Utils.getRandomInt(0, 1000),
                        Utils.getRandomInt(1, 100)
                )
        ).take(Duration.ofSeconds(3));
    }

    @GetMapping("/rest/cities")
    public Flux<ThirdPartyRestService.City> getCities() {
        return restService.getRandomCities()
                .take(Duration.ofSeconds(3));
    }

    @GetMapping("/hell")
    @SneakyThrows
    public Mono<Hell> hell() {
        return Mono.zip(
                findRandomUserJDBC()        .onErrorResume(Throwable.class, t -> Mono.empty()),
                findRandomCommentsJDBC()    .onErrorResume(Throwable.class, t -> Mono.empty()).collectList(),
                findRandomUserJPA()         .onErrorResume(Throwable.class, t -> Mono.empty()),
                findRandomCommentsJPA()     .onErrorResume(Throwable.class, t -> Mono.empty()).collectList(),
                findRandomUserMongo()       .onErrorResume(Throwable.class, t -> Mono.empty()),
                findRandomCommentsMongo()   .onErrorResume(Throwable.class, t -> Mono.empty()).collectList(),
                getCities()                 .onErrorResume(Throwable.class, t -> Mono.empty()).collectList()
        ).map(objects -> {
            final Hell hell = new Hell();
            return objects.mapT1(hell::setUserJdbc)
                    .mapT2(hell::setCommentsJdbc)
                    .mapT3(hell::setUserJpa)
                    .mapT4(hell::setCommentsJpa)
                    .mapT5(hell::setUserMongo)
                    .mapT6(hell::setCommentsMongo)
                    .mapT7(hell::setCities)
                    .getT7();
        }).take(Duration.ofSeconds(5));
    }
}
