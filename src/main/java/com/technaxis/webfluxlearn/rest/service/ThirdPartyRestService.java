package com.technaxis.webfluxlearn.rest.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Random;

/**
 * @author Gordeev Nikita
 * gordeevnm@gmail.com
 * 24.02.2020
 */
@Service
@RequiredArgsConstructor
public class ThirdPartyRestService {
    private final WebClient webClient = WebClient.create("https://api.vk.com");

    public Flux<City> getRandomCities() {
        return webClient.get()
                .uri(getUri())
                .exchange()
                .onErrorResume(Throwable.class, throwable -> Mono.empty())
                .flatMap(response -> response.bodyToMono(Response.class))
                .map(Response::getResponse)
                .flatMapIterable(ResponseBody::getItems);
    }

    public Flux<City> getRandomCitiesRepeatable() {
        return Mono.fromCallable(this::getUri)
                .flatMap(uri -> webClient.get().uri(uri).exchange())
                .flatMap(response -> response.bodyToMono(Response.class))
                .map(Response::getResponse)
                .filter(responseBody -> !responseBody.getItems().isEmpty())
                .repeatWhenEmpty(Flux::repeat)
                .map(ResponseBody::getItems)
                .flatMapMany(Flux::fromIterable)
                .onErrorResume(Throwable.class, throwable -> Mono.empty());
    }

    private String getUri() {
        return "/method/database.getCities" +
                "?country_id=" + rndInt(1, 50) +
                "&count=10" +
                "&offset=" + rndInt(0, 10) +
                "&access_token=cd3715dbcd3715dbcd3715db75cd58f00cccd37cd3715db9364a2d87b4872d78982e9fb" +
                "&v=5.103";
    }

    private final Random random = new Random();

    private int rndInt(int min, int max) {
        return Math.abs(random.nextInt()) % (max - min) + min;
    }

    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Response {
        private ResponseBody response;
    }

    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ResponseBody {
        private int count;
        private List<City> items;
    }

    @Getter
    @Setter
    @ToString
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class City {
        private long id;
        private String title;
    }
}