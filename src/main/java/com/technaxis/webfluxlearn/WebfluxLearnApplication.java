package com.technaxis.webfluxlearn;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import javax.annotation.PostConstruct;

@SpringBootApplication
@RequiredArgsConstructor
public class WebfluxLearnApplication {
    private final MongoMappingContext mongoMappingContext;

    @PostConstruct
    void enableAutoIndex() {
        mongoMappingContext.setAutoIndexCreation(true);
    }

    public static void main(String[] args) {
        System.setProperty("hibernate.types.print.banner", "false");
        SpringApplication.run(WebfluxLearnApplication.class, args);
    }
}
